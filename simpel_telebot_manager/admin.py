from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from simpel_telebot.settings import telebot_settings

from .forms import AdminCampaignForm
from .models import Campaign


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    form = AdminCampaignForm
    readonly_fields = ["user"]
    actions = ["send"]

    @admin.display(description=_("Send selected Campaign messages"))
    def send(self, request, queryset):
        count = queryset.count()
        max = telebot_settings.MAX_ADMIN_BROADCAST_SEND
        if count > max:
            msg_user = f"Max Campaigning messages is {max}."
            self.message_user(request, msg_user)
            return
        try:
            for msg in queryset:
                msg.send()
            msg_user = f"Campaigning of {count} messages has been started"
        except Exception as err:
            msg_user = f"Campaigning failed: {err}"
        self.message_user(request, msg_user)

    def save_model(self, request, obj, form, change):
        return super().save_model(request, obj, form, change)
