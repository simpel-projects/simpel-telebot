from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from simpel_telebot.models import TelegramUser
from simpel_telebot.settings import telebot_settings


class Campaign(models.Model):
    created_at = models.DateTimeField(default=timezone.now, db_index=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    user = models.ForeignKey(
        get_user_model(),
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
    )
    title = models.CharField(
        max_length=255,
        null=True,
        blank=False,
        verbose_name=_("Title"),
    )
    recipients = models.ManyToManyField(
        TelegramUser,
        related_name="broadcasts",
    )
    message = models.TextField(
        verbose_name=_("Messages"),
    )

    class Meta:
        verbose_name = _("Campaign")
        verbose_name_plural = _("Campaigns")

    def __str__(self):
        return str(self.title)

    def send(self):
        from simpel_telebot.handlers.broadcast.utils import send_message
        from simpel_telebot.tasks import broadcast_message

        if telebot_settings.DEBUG:  # for test / debug purposes - run in same thread
            for user in self.recipients.all():
                send_message(
                    user_id=user.user_id,
                    text=self.message,
                )
        else:
            recipient_ids = [user.user_id for user in self.recipients.all()]
            broadcast_message.delay(text=self.message, user_ids=recipient_ids)
