from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path(
        "telebotwebhook/<uuid:uid>/", csrf_exempt(views.ManagedTelebotWebhookView.as_view()), name="managed_botwebhook"
    ),
]
