import json

from django.http import JsonResponse
from django.views.generic import View

from simpel_telebot.settings import telebot_settings
from simpel_telebot.tasks import process_telegram_event_for_bot
from simpel_telebot.utils import get_logger

logger = get_logger()


class TelebotWebhookView(View):
    # WARNING: if fail - Telegram webhook will be delivered again.
    # Can be fixed with async celery task execution
    def post(self, request, uid, *args, **kwargs):
        data = json.loads(request.body)
        if telebot_settings.DEBUG:
            process_telegram_event_for_bot(uid, data)
            # process_telegram_event(json.loads(request.body))
        else:
            # Process Telegram event in worker (async)
            process_telegram_event_for_bot.delay(uid, data)
        # e.g. remove buttons, typing event
        return JsonResponse({"ok": "POST request processed"})

    def get(self, request, *args, **kwargs):  # for debug
        return JsonResponse({"ok": "Get request received! But nothing done"})
