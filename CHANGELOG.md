# CHANGELOG

## V1.0.0

    Goal:
        - Release simpel ERP Baristand

## V2.0.0

Pemisahan Module Laboratorium Baristand
Module Baristand:

- simpelbar (Module Core Simpel Baristand)
  Pemisahan komponen simpel yang spesifik pada simpel baristand, seperti parameter, metode uji serifikat dan lain-lain.
- mod_laboratorium (Laboratorium Pengujian)
- mod_inspection (Inspeksi Teknis)
- simpel_certification (Sertifikasi Produk)
- simpel_calibration (Kalibrasi)
- mod_research (Penelitian dan Pengembangan)
- mod_consultancy (Konsultansi)
- mod_training (Pelatihan)

Goal:
    Simpel V.2 dapat berdiri sendiri tanpa komponen/modul yang terkait laboratorium baristand
