import json

from simpel_telebot.tasks import process_telegram_event

# from django.urls import reverse

# import requests


def run():
    # uid = "ae35713b-798e-4969-80b1-0add2561f939"
    # endpoint = reverse("telebot_webhook")
    # endpoint = reverse("telebot_webhook", args=(uid,))
    # print(json.loads(update))
    # resp = requests.post("http://127.0.0.1:8000%s" % endpoint, json=json.loads(update))
    # print(resp)
    data = """
    {
        "update_id":10000,
        "message":{
        "date":1441645532,
        "chat":{
            "last_name":"Test Lastname",
            "id":1111111,
            "first_name":"Test",
            "username":"Test"
        },
        "message_id":1365,
        "from":{
            "last_name":"Test Lastname",
            "id":1111111,
            "first_name":"Test",
            "username":"Test"
        },
        "text":"/start"
        }
    }
    """
    process_telegram_event(json.loads(data))
