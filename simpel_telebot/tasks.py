"""
    Task periodically from admin panel via django-celery-beat
"""

import time

import telegram
from django_rq import job
from telegram import Update

from .handlers.broadcast.utils import from_celery_entities_to_entities, from_celery_markup_to_markup, send_message
from .utils import get_logger

logger = get_logger()


@job
def process_telegram_event(update_json):
    from .telebot import get_bot
    bot, dp = get_bot()
    update = Update.de_json(update_json, bot)
    logger.info(update_json)
    dp.process_update(update)


@job
def broadcast_message(
    user_ids,
    text,
    entities=None,
    reply_markup=None,
    sleep_between=0.4,
    parse_mode=telegram.ParseMode.HTML,
):
    """It's used to broadcast message to big amount of users"""
    logger.info(f"Going to send message: '{text}' to {len(user_ids)} users")

    entities_ = from_celery_entities_to_entities(entities)
    reply_markup_ = from_celery_markup_to_markup(reply_markup)
    for user_id in user_ids:
        try:
            send_message(
                user_id=user_id,
                text=text,
                entities=entities_,
                parse_mode=parse_mode,
                reply_markup=reply_markup_,
            )
            logger.info(f"Broadcast message was sent to {user_id}")
        except Exception as err:
            # logger.error(f"Failed to send message to {user_id}, reason: {e}")
            print(err)
            pass
        time.sleep(max(sleep_between, 0.1))

    logger.info("Broadcast finished!")


@job
def save_data_from_arcgis(latitude, longitude, location_id):
    from .models import Arcgis

    Arcgis.from_json(Arcgis.reverse_geocode(latitude, longitude), location_id=location_id)
