from datetime import timedelta

from django.utils.timezone import now
from telegram import ParseMode, Update
from telegram.ext import CallbackContext

from ...models import TelegramUser
from . import static_text
from .utils import get_csv_from_qs_values


def admin(update: Update, context: CallbackContext):
    """Show help info about all secret admins commands"""
    u = TelegramUser.get_user(update, context)
    if not u.is_admin:
        update.message.reply_text(static_text.only_for_admins)
        return
    update.message.reply_text(static_text.secret_admin_commands)


def stats(update: Update, context: CallbackContext):
    """Show help info about all secret admins commands"""

    u = TelegramUser.get_user(update, context)
    if not u.is_admin:
        update.message.reply_text(static_text.only_for_admins)
        return

    text = static_text.users_amount_stat.format(
        user_count=TelegramUser.objects.count(),  # count may be ineffective if there are a lot of users.
        active_24=TelegramUser.objects.filter(updated_at__gte=now() - timedelta(hours=24)).count(),
    )

    update.message.reply_text(
        text,
        parse_mode=ParseMode.HTML,
        disable_web_page_preview=True,
    )


def export_users(update: Update, context: CallbackContext):
    u = TelegramUser.get_user(update, context)
    if not u.is_admin:
        update.message.reply_text(static_text.only_for_admins)
        return

    # in values argument you can specify which fields should be returned in output csv
    users = TelegramUser.objects.all().values()
    csv_users = get_csv_from_qs_values(users)
    context.bot.send_document(chat_id=u.user_id, document=csv_users)
