from django import forms
from django.contrib import messages
from django.contrib.admin.widgets import FilteredSelectMultiple

from ckeditor.widgets import CKEditorWidget

from .handlers.broadcast.utils import send_message
from .models import TelegramUser
from .settings import telebot_settings
from .tasks import broadcast_message


class BroadcastForm(forms.Form):
    recipients = forms.ModelMultipleChoiceField(
        queryset=TelegramUser.objects.all(),
        widget=FilteredSelectMultiple("verbose name", is_stacked=False),
    )
    broadcast_text = forms.CharField(widget=CKEditorWidget())

    def send(self, request):
        data = self.cleaned_data
        message = data["broadcast_text"]
        recipients = data["recipients"]
        recipients_count = recipients.count()
        success_message = f"Broadcasting of {recipients_count} messages has been started"
        if telebot_settings.DEBUG:  # for test / debug purposes - run in same thread
            for user in recipients:
                send_message(
                    user_id=user.user_id,
                    text=message,
                )
        else:
            recipient_ids = [user.user_id for user in recipients]
            broadcast_message.delay(text=message, user_ids=recipient_ids)
        messages.info(request, success_message)
