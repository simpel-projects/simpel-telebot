from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from simpel_telebot.telebot import run_pooling

User = get_user_model()


class Command(BaseCommand):
    help = "Start telebot pooling!"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        run_pooling()
