from django.contrib import admin
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _

from simpel_telebot.forms import BroadcastForm
from simpel_telebot.views import BroadcastView

from .models import Arcgis, Location, TelegramBot, TelegramUser


def user_staff_or_admin(user):
    return


@method_decorator(
    [
        login_required,
        user_passes_test(test_func=lambda x: x.is_staff or x.is_superuser),
    ],
    name="dispatch",
)
class AdminBroadcastView(BroadcastView):
    template_name = "admin/simpel_telebot/broadcast_form.html"
    form_class = BroadcastForm
    model_admin = None

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "title": _("Telebot Campaign Message"),
                **admin.site.each_context(self.request),
            }
        )
        return context

    def post(self, request, *args, **kwargs):
        cancel = request.POST.get("_cancel", None)
        if cancel is not None:
            return redirect("admin:simpel_telebot_telegramuser_changelist")
        return super().post(request, *args, **kwargs)


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = [
        "user_id",
        "username",
        "language_code",
        "deep_link",
        "created_at",
        "updated_at",
        "is_blocked_bot",
    ]
    list_filter = [
        "is_blocked_bot",
    ]
    search_fields = ("username", "user_id")


@admin.register(TelegramBot)
class TelegramBotAdmin(admin.ModelAdmin):
    list_display = ["id", "uid", "token", "bot_name"]


@admin.register(Arcgis)
class ArcgisAdmin(admin.ModelAdmin):
    list_display = ("location", "city", "country_code")
    list_filter = ("country_code",)


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ("user", "longitude", "latitude")
