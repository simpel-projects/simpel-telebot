from django.urls import path

from . import admin

urlpatterns = [
    path("", admin.AdminBroadcastView.as_view(), name="admin_telebot_broadcast"),
]
