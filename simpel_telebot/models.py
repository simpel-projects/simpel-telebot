from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

import requests

from .handlers.utils.info import extract_user_data_from_update
from .managers import GetOrNoneManager
from .settings import telebot_settings
from .utils import generate_uid, get_logger

nb = dict(null=True, blank=True)
logger = get_logger()


class TelegramBot(models.Model):

    owner = models.ForeignKey(
        get_user_model(),
        related_name=_("telegram_bots"),
        on_delete=models.SET_NULL,
        **nb,
    )
    uid = models.CharField(
        max_length=8,
        verbose_name=_("UID"),
        editable=False,
    )
    token = models.CharField(
        max_length=255,
        unique=True,
        verbose_name=_("Bot Token"),
    )
    bot_id = models.CharField(
        max_length=255,
        verbose_name=_("Bot ID"),
        null=True,
        blank=True,
    )
    bot_name = models.CharField(
        max_length=255,
        verbose_name=_("Bot Name"),
        null=True,
        blank=True,
    )
    data = models.JSONField(null=True, blank=True)
    created_at = models.DateTimeField(
        default=timezone.now,
        db_index=True,
        editable=False,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        editable=False,
    )

    def __str__(self):
        return str(self.bot_name)

    def clean(self):
        # clean token makesure bot token is exist.
        pass

    def save(self, *args, **kwargs):
        self.uid = generate_uid(self.__class__)
        super().save(*args, **kwargs)


class TelegramUser(models.Model):
    owner = models.ForeignKey(
        get_user_model(),
        related_name=_("telegram_accounts"),
        on_delete=models.SET_NULL,
        **nb,
    )
    user_id = models.PositiveBigIntegerField(unique=True)  # telegram_id
    username = models.CharField(max_length=32, **nb)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256, **nb)
    language_code = models.CharField(max_length=8, help_text="Telegram client's lang", **nb)
    deep_link = models.CharField(max_length=64, **nb)

    is_blocked_bot = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    data = models.JSONField(null=True, blank=True)
    created_at = models.DateTimeField(
        default=timezone.now,
        db_index=True,
        editable=False,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        editable=False,
    )

    def __str__(self):
        return f"@{self.username}" if self.username is not None else f"{self.user_id}"

    @classmethod
    def get_user_and_created(cls, update, context):
        """python-telegram-bot's Update, Context --> TelegramUser instance"""
        data = extract_user_data_from_update(update)
        teleuser, created = TelegramUser.objects.get_or_create(user_id=data["user_id"], defaults=data)
        # # check.save()
        # u, created = cls.objects.update_or_create(user_id=data["user_id"], defaults=data)
        if created:
            # Save deep_link to TelegramUser model
            if context is not None and context.args is not None and len(context.args) > 0:
                payload = context.args[0]
                if str(payload).strip() != str(data["user_id"]).strip():  # you can't invite yourself
                    teleuser.deep_link = payload
                    teleuser.save()

        return teleuser, created

    @classmethod
    def get_user(cls, update, context):
        teleuser, _ = cls.get_user_and_created(update, context)
        return teleuser

    @classmethod
    def get_user_by_username_or_user_id(cls, username_or_user_id):
        """Search user in DB, return TelegramUser or None if not found"""
        username = str(username_or_user_id).replace("@", "").strip().lower()
        if username.isdigit():  # user_id
            return cls.objects.filter(user_id=int(username)).first()
        return cls.objects.filter(username__iexact=username).first()

    @property
    def invited_users(self):
        return TelegramUser.objects.filter(deep_link=str(self.user_id), created_at__gt=self.created_at)

    @property
    def tg_str(self):
        if self.username:
            return f"@{self.username}"
        return f"{self.first_name} {self.last_name}" if self.last_name else f"{self.first_name}"


class TelegramChat(models.Model):
    user_id = models.PositiveBigIntegerField(unique=True)  # telegram_id
    data = models.JSONField(null=True, blank=True)
    created_at = models.DateTimeField(
        default=timezone.now,
        db_index=True,
        editable=False,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        editable=False,
    )


class Location(models.Model):
    created_at = models.DateTimeField(default=timezone.now, db_index=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
    latitude = models.FloatField()
    longitude = models.FloatField()

    objects = GetOrNoneManager()

    def __str__(self):
        return f"user: {self.user}, created at {self.created_at.strftime('(%H:%M, %d %B %Y)')}"

    def save(self, *args, **kwargs):
        super(Location, self).save(*args, **kwargs)
        # Parse location with arcgis
        from .tasks import save_data_from_arcgis

        if telebot_settings.DEBUG:
            save_data_from_arcgis(latitude=self.latitude, longitude=self.longitude, location_id=self.pk)
        else:
            save_data_from_arcgis.delay(latitude=self.latitude, longitude=self.longitude, location_id=self.pk)


class Arcgis(models.Model):
    created_at = models.DateTimeField(default=timezone.now, db_index=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    location = models.OneToOneField(
        Location,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    match_addr = models.CharField(max_length=200)
    long_label = models.CharField(max_length=200)
    short_label = models.CharField(max_length=128)

    addr_type = models.CharField(max_length=128)
    location_type = models.CharField(max_length=64)
    place_name = models.CharField(max_length=128)

    add_num = models.CharField(max_length=50)
    address = models.CharField(max_length=128)
    block = models.CharField(max_length=128)
    sector = models.CharField(max_length=128)
    neighborhood = models.CharField(max_length=128)
    district = models.CharField(max_length=128)
    city = models.CharField(max_length=64)
    metro_area = models.CharField(max_length=64)
    subregion = models.CharField(max_length=64)
    region = models.CharField(max_length=128)
    territory = models.CharField(max_length=128)
    postal = models.CharField(max_length=128)
    postal_ext = models.CharField(max_length=128)

    country_code = models.CharField(max_length=32)

    lng = models.DecimalField(max_digits=21, decimal_places=18)
    lat = models.DecimalField(max_digits=21, decimal_places=18)

    objects = GetOrNoneManager()

    def __str__(self):
        return f"{self.location}, city: {self.city}, country_code: {self.country_code}"

    @classmethod
    def from_json(cls, arcgis_json, location_id):
        address, location = arcgis_json.get("address"), arcgis_json.get("location")

        if "address" not in arcgis_json or "location" not in arcgis_json:
            return

        arcgis_data = {
            "match_addr": address.get("Match_addr"),
            "long_label": address.get("LongLabel"),
            "short_label": address.get("ShortLabel"),
            "addr_type": address.get("Addr_type"),
            "location_type": address.get("Type"),
            "place_name": address.get("PlaceName"),
            "add_num": address.get("AddNum"),
            "address": address.get("Address"),
            "block": address.get("Block"),
            "sector": address.get("Sector"),
            "neighborhood": address.get("Neighborhood"),
            "district": address.get("District"),
            "city": address.get("City"),
            "metro_area": address.get("MetroArea"),
            "subregion": address.get("Subregion"),
            "region": address.get("Region"),
            "territory": address.get("Territory"),
            "postal": address.get("Postal"),
            "postal_ext": address.get("PostalExt"),
            "country_code": address.get("CountryCode"),
            "lng": location.get("x"),
            "lat": location.get("y"),
        }

        arc, _ = cls.objects.update_or_create(location_id=location_id, defaults=arcgis_data)
        return

    @staticmethod
    def reverse_geocode(lat: float, lng: float):
        r = requests.post(
            "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode",
            params={
                "f": "json",
                "location": f"{lng}, {lat}",
                "distance": 50000,
                "outSR": "",
            },
            headers={
                "Content-Type": "application/json",
            },
        )
        return r.json()
