# Generated by Django 3.2.7 on 2022-03-04 09:40

import django.db.models.deletion
import django.db.models.manager
import django.utils.timezone
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TelegramUser',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user_id', models.PositiveBigIntegerField(unique=True)),
                ('username', models.CharField(blank=True, max_length=32, null=True)),
                ('first_name', models.CharField(max_length=256)),
                ('last_name', models.CharField(blank=True, max_length=256, null=True)),
                ('language_code', models.CharField(blank=True, help_text="Telegram client's lang", max_length=8, null=True)),
                ('deep_link', models.CharField(blank=True, max_length=64, null=True)),
                ('is_blocked_bot', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='telegram_accounts', to=settings.AUTH_USER_MODEL)),
                ('id', models.BigAutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
            managers=[
                ('admins', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(db_index=True, default=django.utils.timezone.now, editable=False)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='simpel_telebot.telegramuser')),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
        ),
        migrations.AlterModelManagers(
            name='telegramuser',
            managers=[
            ],
        ),
        migrations.AlterField(
            model_name='telegramuser',
            name='created_at',
            field=models.DateTimeField(db_index=True, default=django.utils.timezone.now),
        ),
        migrations.CreateModel(
            name='Arcgis',
            fields=[
                ('created_at', models.DateTimeField(db_index=True, default=django.utils.timezone.now, editable=False)),
                ('location', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='simpel_telebot.location')),
                ('match_addr', models.CharField(max_length=200)),
                ('long_label', models.CharField(max_length=200)),
                ('short_label', models.CharField(max_length=128)),
                ('addr_type', models.CharField(max_length=128)),
                ('location_type', models.CharField(max_length=64)),
                ('place_name', models.CharField(max_length=128)),
                ('add_num', models.CharField(max_length=50)),
                ('address', models.CharField(max_length=128)),
                ('block', models.CharField(max_length=128)),
                ('sector', models.CharField(max_length=128)),
                ('neighborhood', models.CharField(max_length=128)),
                ('district', models.CharField(max_length=128)),
                ('city', models.CharField(max_length=64)),
                ('metro_area', models.CharField(max_length=64)),
                ('subregion', models.CharField(max_length=64)),
                ('region', models.CharField(max_length=128)),
                ('territory', models.CharField(max_length=128)),
                ('postal', models.CharField(max_length=128)),
                ('postal_ext', models.CharField(max_length=128)),
                ('country_code', models.CharField(max_length=32)),
                ('lng', models.DecimalField(decimal_places=18, max_digits=21)),
                ('lat', models.DecimalField(decimal_places=18, max_digits=21)),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Broadcast',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField(verbose_name='Messages')),
                ('recipients', models.ManyToManyField(related_name='broadcasts', to='simpel_telebot.TelegramUser')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('created_at', models.DateTimeField(db_index=True, default=django.utils.timezone.now, editable=False)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='Title')),
            ],
            options={
                'verbose_name': 'Broadcast',
                'verbose_name_plural': 'Broadcasts',
            },
        ),
        migrations.AlterField(
            model_name='telegramuser',
            name='created_at',
            field=models.DateTimeField(db_index=True, default=django.utils.timezone.now, editable=False),
        ),
    ]
