from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path("telebotwebhook/", csrf_exempt(views.TelebotWebhookView.as_view()), name="telebot_webhook"),
]
