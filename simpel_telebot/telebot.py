"""
    Telegram event handlers
"""
import logging
import sys

from telegram import Bot, error as telegram_errors
from telegram.ext import Dispatcher, Updater

from .helpers import setup_commands, setup_dispatcher
from .settings import telebot_settings


# Global variable - best way I found to init Telegram bot
def get_bot():
    bot = Bot(telebot_settings.BOT_TOKEN)

    try:
        bot.get_me()["username"]
    except telegram_errors.Unauthorized:
        logging.error("Invalid TELEGRAM_TOKEN.")
        sys.exit(1)

    # WARNING: it's better to comment the line below in DEBUG mode.
    # Likely, you'll get a flood limit control error, when restarting bot too often

    setup_commands(bot)

    dispatcher = setup_dispatcher(
        Dispatcher(
            bot,
            update_queue=None,
            workers=telebot_settings.WORKERS_NUMBER,
            use_context=True,
        )
    )

    return bot, dispatcher


def run_pooling():
    """Run bot in pooling mode"""
    updater = Updater(telebot_settings.BOT_TOKEN, use_context=True)

    dp = updater.dispatcher
    dp = setup_dispatcher(dp)

    bot_info = Bot(telebot_settings.BOT_TOKEN).get_me()
    bot_link = "https://t.me/%s" % bot_info["username"]

    print(f"Pooling of '{bot_link}' started")
    # it is really useful to send '👋' emoji to developer
    # when you run local test
    # bot.send_message(text='👋', chat_id=<YOUR TELEGRAM ID>)

    updater.start_polling()
    updater.idle()
